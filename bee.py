import minimalmodbus
import serial.tools.list_ports
import time
import sys
import time
from influxdb import InfluxDBClient

BACKEND = "influx"

SLEEP_TIME = 30


BAUDRATE = 115200


INFLUX_HOST = 'bee-db-influx'
INFLUX_PORT = 8086
INFLUX_USER = 'bee'
INFLUX_PASS = 'bee'
INFLUX_DB = 'weather'

mb = None
minimalmodbus.TIMEOUT = 10
bee_data = {}



def FindDevice():
    global mb

    mb = minimalmodbus.Instrument("/dev/bee", 0x80, mode='ascii')
    if not mb.serial.is_open:
        mb.serial.open()
    print('Device Found!')
    return True
    
    return False


FindDevice()



def get_dragonfly_data():
    for i in range(0,1):
        start_reg = 8 * i

        address      = mb.read_register(start_reg + 0, numberOfDecimals=0, functioncode=3, signed=False)
        status       = mb.read_register(start_reg + 1, functioncode=0x03)
        current      = mb.read_register(start_reg + 2, functioncode=0x03)
        power        = mb.read_register(start_reg + 3, functioncode=0x03)
        voltage      = mb.read_register(start_reg + 4, functioncode=0x03)
        light        = mb.read_register(start_reg + 5, functioncode=0x03)
        temp_1       = mb.read_register(start_reg + 6, functioncode=0x03)
        humi_1       = mb.read_register(start_reg + 7, functioncode=0x03)
        temp_2       = mb.read_register(start_reg + 8, functioncode=0x03)
        humi_2       = mb.read_register(start_reg + 9, functioncode=0x03)
        pressure     = mb.read_register(start_reg + 10, functioncode=0x03)
        acc_voltage  = mb.read_register(start_reg + 11, functioncode=0x03)
        rssi         = mb.read_register(start_reg + 12, functioncode=0x03)
        
        if (rssi & 0x8000):
            rssi = (rssi & 0x7FFF) * -1
        if (temp_1 & 0x8000):
            temp_1 = (temp_1 & 0x7FFF) * -1
        if (temp_2 & 0x8000):
            temp_2 = (temp_2 & 0x7FFF) * -1        

        bee_data['address'] = address    
        bee_data['voltage'] = voltage
        bee_data['current'] = float(current / 100.0)
        bee_data['power'] = float(power*10.0)
        bee_data['light'] = (light*2)
        bee_data['temp1'] = float((temp_1 / 10.0))
        bee_data['temp2'] = float((temp_2 / 10.0))
        bee_data['humi1'] = humi_1
        bee_data['humi2'] = humi_2
        bee_data['pressure'] = pressure
        bee_data['acc_voltage'] = acc_voltage
        bee_data['rssi'] = rssi
        


        print('Bee data')
        print('\n')
        print('address:   %d'       % int(bee_data['address']))
        print('voltage:  %d\tmV'  % (bee_data['voltage']))
        print('current:  %.2f\tmA' % float(bee_data['current']))
        print('power:    %d\tuW' % int(bee_data['power']))
        print('light:    %d\tlux'  % int(bee_data['light']))
        print('temp_1:   %.1f\tC'  % float(bee_data['temp1']))
        print('temp_2:   %.1f\tC'  % float(bee_data['temp2']))
        print('humi_1:   %d\t%%rH' % int(bee_data['humi1']))
        print('humi_2:   %d\t%%rH' % int(bee_data['humi2']))
        print('pressure: %d\thPa'  % int(bee_data['pressure']))
        print('acc_voltage:    %d\tmV'  % int(bee_data['acc_voltage']))
        print('rssi:     %d'       % int(bee_data['rssi']))
        print('\n')
    
    time.sleep(1)

def insert_influx(weather):
    client = InfluxDBClient(host=INFLUX_HOST,
                            port=INFLUX_PORT,
                            username=INFLUX_USER,
                            password=INFLUX_PASS)
    databases = [db['name'] for db in client.get_list_database()]
    if INFLUX_DB not in databases:
        print('Database {db} not found.'.format(db=INFLUX_DB))
        if not client.create_database(INFLUX_DB):
            print('Cannot create database {db}.'.format(db=INFLUX_DB))
            sys.exit(1)
        print('Database {db} created.'.format(db=INFLUX_DB))
    client.switch_database(INFLUX_DB)
            
            
    json_body = [
        {
            "measurement": "weather",
            "tags": {
                "station-id": 'bee1'
            },
            "fields": {
                "temp1":        float(bee_data['temp1']),
                "temp2":        float(bee_data['temp2']),
                "humi1":        int(bee_data['humi1']),
                "humi2":        int(bee_data['humi2']),
                "light":        int(bee_data['light']),
                "pressure":     int(bee_data['pressure'])
            }
        }
    ]
    if not client.write_points(json_body):
        print('Cannot write weather data into database!')
        sys.exit(1)
    # Writing technical information
    json_body = [
        {
            "measurement": "info",
            "tags": {
                "station-id": 'bee1'
            },
            "fields": {
                "address":      int(bee_data['address']),
                "rssi":         int(bee_data['rssi']),
                "voltage":      int(bee_data['voltage']),
                "acc_voltage":  int(bee_data['acc_voltage']),
                "power":        int(bee_data['power']),
                "current":      int(bee_data['current'])
            }
        }
    ]
    if not client.write_points(json_body):
        print('Cannot write tech data into database!')
        sys.exit(1)
    
def delete_db():
    client = InfluxDBClient(host=INFLUX_HOST,
                        port=INFLUX_PORT,
                        username=INFLUX_USER,
                        password=INFLUX_PASS)
    databases = [db['name'] for db in client.get_list_database()]
    client.drop_database(INFLUX_DB)
    print('DB dropped!')

   


def main():

    
    bee_data = {}
    while (1):
        get_dragonfly_data()
        insert_influx(bee_data)
	    #print("Written to influx: %s" % bee_data)
        time.sleep(SLEEP_TIME)
        pass
    

if __name__ == '__main__':
    main()